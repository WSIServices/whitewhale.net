[WhiteWhale][0] Projects README
===============================

[PHP projects][1] for inclusion in Git projects.

[XPHP][2]
---------

A free class for PHP which allows for the embedding of dynamic content (web application variables, function output, etc.) in a web page, using a pure XML-based syntax.

[Smart Cache for PHP][3]
------------------------

An implementation of a smart caching system designed to optimize the serving of dynamic web pages.

[ResourceOptimizer][4]
----------------------

Applies common resource optimization techniques to a PHP page, including aggregation of scripts and stylesheets, minimization, compression, deferred loading, distribution across CDNs, and extreme cacheability.

Requirements
------------

Requirements have not been tested.

Developed on PHP 5.3.2

Installation
------------

Cloning the projects git repository or downloading from the [repository project page][5] can provide you with the most up-to-date code and is currently the only methods for obtaining the package.

> git clone https://bitbucket.org/WSIServices/whitewhale.net.git

Documentation
-------------

Documentation is not available at this time, however the code is commented in PhpDocumentor format and as time allows documentation will be generated.

[0]: http://whitewhale.net
[1]: http://technologies.whitewhale.net
[2]: http://technologies.whitewhale.net/xphp
[3]: http://technologies.whitewhale.net/cache
[4]: http://technologies.whitewhale.net/resource_optimizer
[5]: http://bitbucket.wsi-services.com/phisux
