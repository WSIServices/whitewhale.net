<?php

/*

Resource Optimizer for PHP

This is an open source resource optimizer developed by Alexander Romanovich for White Whale Web Services (http://www.whitewhale.net).
This code may be freely distributed, but please retain this comment section.
Please see: http://technologies.whitewhale.net/resource_optimizer/ for documentation.

*/

class ResourceOptimizer {
protected $aggregates; // array of items to aggregate
protected $url; // absolute url from web root to resource optimizer directory
protected $server_path; // full system path to resource optimizer directory
protected $no_defers; // status of JS deferrals
public $error; // error text for any runtime errors
public $enable_deferred_load=true; // toggle for deferred loading of resources
public $url_cache_ttl=3600; // ttl for url cache entries (default: 3600)
public $resource_cache_ttl=2592000; // ttl for resource entries (default: 2592000)
public $head_cache_ttl=300; // ttl for head cache entries (default: 300)
public $CDNS=array(); // CDNs that resources should be distributed over (must point to shared docroot)

public function __construct($dir) { // initializes the resource optimizer
$this->document_root=substr($_SERVER['SCRIPT_FILENAME'],0,-1*strlen(str_replace('//','/',$_SERVER['SCRIPT_NAME']))); // get document root
$this->ro_path=$this->document_root.$dir; // set RO server path
$this->url=$dir; // set url to ResourceOptimizer directory
}

public function checkInstallation() { // checks the ResourceOptimizer installation
if (!extension_loaded('dom')) $this->error='The DOM extension for PHP must be available.'; // check if DOM is available
if (empty($this->error)) if (!function_exists('shell_exec') || !is_callable('shell_exec')) $this->error='The function "shell_exec" must be available.'; // check if shell_exec is available
if (empty($this->error)) if (!file_exists($this->ro_path.'/index.php') || !file_exists($this->ro_path.'/.htaccess') || !file_exists($this->ro_path.'/includes/class.resource_optimizer.php') || !file_exists($this->ro_path.'/includes/yuicompressor.jar')) $this->error='Could not find complete ResourceOptimizer install at: '.$this->ro_path.'.'; // ensure resource optimizer directory can be found
if (empty($this->error)) if (!@is_writable($this->ro_path.'/cache') || !@is_writable($this->ro_path.'/cache/resources') || !@is_writable($this->ro_path.'/cache/urls') && !@is_writable($this->ro_path.'/cache/heads')) $this->error=$this->ro_path.'/cache and its subdirectories (resources, urls, heads) must be writable by the web server.'; // ensure cache directory is writable
}

public function optimizeResources($buffer) { // aggregates resources in supplied page source
if (empty($this->error)) if (extension_loaded('dom') && function_exists('shell_exec') && is_callable('shell_exec')) { // if DOM extension and shell_exec available
	$matches=array(); // init array for matching head
	preg_match('/(<head>.+?<\/head>)/s',$buffer,$matches); // find the head element
	if (!empty($matches[1])) { // if head found
		if (!isset($this->xml)) $this->xml=new DOMDocument; // create new DOM document
		if (@$this->xml->loadXML($matches[1])) if ($this->xml->documentElement->hasChildNodes()) { // if DOM loads and root node contains children
			$this->getResources(); // scan head for resources that can potentially be aggregated
			if (!empty($this->head_hash)) { // if there was a head hash
				if (!isset($this->has_apc)) $this->has_apc=extension_loaded('apc'); // if APC available
				if ($this->head_cache=$this->getHeadCache()) { // if a head cache was found
					$head=str_replace(array_keys($this->head_cache),array_values($this->head_cache),str_replace(' />','/>',$matches[1])); // get new head
					if (@$this->xml->loadXML($head)) return $this->formatDocument($matches[1],$buffer); // if new head loads, return transformed document
				};
				if (!empty($this->aggregates)) $this->aggregateResources(); // aggregate the document's resources
				return $this->formatDocument($matches[1],$buffer); // return transformed document
			};
		};
	}
	else $this->error='No head element found in document.'; // else if no head element found, give error
}
else $this->error='DOM extension must be loaded, and the function "shell_exec" must be callable.'; // else if DOM extension or shell_exec not available, give error
return preg_replace('/\s*data-(no-aggregation|aggregate-break)="[^"]+"/','',$buffer); // return buffer with aggregated resources
}

protected function getResources() { // gets resources that can be used by the optimizer
$this->head_hash=''; // init head hash
$this->aggregates=array('js'=>array(),'css'=>array()); // init aggregates array
$this->no_defers=false; // reset deferral status
$this->no_moves=false; // reset move status
foreach($this->xml->documentElement->childNodes as $node) { // loop through child nodes
	switch($node->nodeName) { // branch by node name
		case 'link': // if link element found
			$href=@$node->getAttribute('href'); // get href
			if (@$node->getAttribute('type')=='text/css' && @$node->getAttribute('rel')=='stylesheet') if (!empty($href) && !isset($in_conditional) && @$node->getAttribute('data-no-aggregation')!=='true') { // if CSS, if valid resource
				$media=@$node->getAttribute('media'); // get media
				$is_break=((isset($last_media) && $last_media!=$media) || $node->getAttribute('data-aggregate-break')==='true'); // set break flag, considering no media attribute and screen the same
				$this->aggregates['css'][]=array('node'=>$node,'is_break'=>$is_break,'media'=>$media); // record resource
				$this->head_hash.='css_'.$href.'_'.$media."\n"; // append this resource to the head hash
				$last_media=$media; // update last_media
			}
			else $this->aggregates['css'][]=false; // else if not a valid resource, flag resource as not aggregated
			break;
		case 'script': // if script element found
			$src=@$node->getAttribute('src'); // get src
			if (!empty($src) && @$node->getAttribute('type')=='text/javascript' && $node->textContent=='' && !isset($in_conditional) && @$node->getAttribute('data-no-aggregation')!=='true') { // if valid resource
				$this->aggregates['js'][]=array('node'=>$node,'is_break'=>@$node->getAttribute('data-aggregate-break')==='true'); // record resource
				$this->head_hash.='js_'.$src."\n"; // append this resource to the head hash
			}
			else { // else if not a valid resource
				$this->aggregates['js'][]=false; // flag resource as not aggregated
				if ($this->enable_deferred_load) $this->no_defers=true; // disable defer attribute
			};
			break;
		case '#comment': // if comment element found
			if (preg_match('/\[\s*if\s+[^\s]+\s.+?\]/s',$node->textContent)) { // if this is a conditional start
				$in_conditional=true; // set conditional flag
				if ($this->enable_deferred_load && strpos($node->textContent,'<script ')!==false) { // if defers enabled, if comment contains a script
					$this->no_defers=true; // // disable defers and moves
					$this->no_moves=true;
				};
			};
			if (isset($in_conditional)) if (preg_match('/\[\s*endif\s*\]/s',$node->textContent)) unset($in_conditional); // if this is a conditional end, clear conditional flag
			break;
	};
};
if (isset($last_media)) unset($last_media); // reset last_media
$this->head_hash=hash('md5',$this->head_hash); // format head hash
}

protected function getHeadCache() { // gets the head cache if there is one
$cache=false; // default to no cache
if ($this->has_apc) $cache=apc_fetch('ro_head_'.$this->head_hash); // fetch head cache from APC
if (empty($cache)) if (@filemtime($this->ro_path.'/cache/heads/'.$this->head_hash[0].'/'.$this->head_hash)>$_SERVER['REQUEST_TIME']-$this->head_cache_ttl) if ($cache=file_get_contents($this->ro_path.'/cache/heads/'.$this->head_hash[0].'/'.$this->head_hash)) $cache=@unserialize($cache); // fetch head cache from FS if not available via APC
return $cache;
}

protected function aggregateResources() { // aggregates resources in the document
$this->head_cache=array(); // if no content was rejected, init head cache content
$this->expired_urls=array(); // init expired urls array
$md5s=array(); // init array of resource hashes
foreach($this->aggregates as $type=>$resources) foreach($resources as $rkey=>$resource) if ($resource!==false) if ($response=$this->getContentForResource($resource,$type)) {$this->aggregates[$type][$rkey]=$response;} // if valid content at resource urls, register and cache content
else $this->aggregates[$type][$rkey]=false; // else if content not approved, flag resource as non-aggregated
foreach($this->aggregates as $type=>$resources) { // for each type, if there were resources
	$aggregates=array(); // init array of resource groups
	$nodes=array(); // init array of nodes for each resource group
	$aggregate_id=''; // init id of resource group
	foreach($resources as $rkey=>$resource) { // loop through resources
		if ($resource===false) { // if skipped resource
			if (!empty($nodes)) { // if there were resources in this group so far
				$aggregates[hash('md5',$aggregate_id)]=$nodes; // add the resources
				$nodes=array(); // reset the nodes array and resource id
				$aggregate_id='';
			};
		}
		else if (!empty($resource['md5'])) if (!isset($md5s[$resource['md5']])) { // else if not skipped and resource has not been added before
			if (!empty($resource['is_break'])) { // if resource is breaking the chain
				if (!empty($nodes)) { // if there were resources in this group so far
					$aggregates[hash('md5',$aggregate_id)]=$nodes; // add the resources
					$nodes=array(); // reset the nodes array and resource id
					$aggregate_id='';
				};
			};
			$nodes[$resource['md5']]=$resource; // add resource
			$md5s[$resource['md5']]=''; // flag resource as having been included
			$aggregate_id.=$resource['md5']; // append resource hash to aggregate hash
		}
		else $this->xml->documentElement->removeChild($resource['node']); // else if resource already included, remove the resource from the DOM
	};
	if (!empty($nodes)) { // if there were resources in this group so far
		$aggregates[hash('md5',$aggregate_id)]=$nodes; // add the resources
		$nodes=array(); // reset the nodes array and resource id
		$aggregate_id='';
	};
	foreach($aggregates as $aggregate_id=>$entries) { // loop through aggregates
		$filename=$aggregate_id.'.'.$type; // get filename for aggregated resource (unique by hash of content)
		if ($mtime=@filemtime($this->ro_path.'/cache/resources/'.$filename[0].'/'.$filename)) { // if aggregated resource is already cached
			if ($mtime<=$_SERVER['REQUEST_TIME']-$this->resource_cache_ttl) touch($this->ro_path.'/cache/resources/'.$filename[0].'/'.$filename); // if expired, update the timestamp of aggregated resource to prevent pruning later
		}
		else { // else if caching for the first time
			if (!is_dir($this->ro_path.'/cache/resources/'.$filename[0])) @mkdir($this->ro_path.'/cache/resources/'.$filename[0]); // make resources sub dir if it doesn't exist
			$output=''; // init output for aggregate
			foreach($entries as $entry) $output.=@file_get_contents($this->ro_path.'/cache/urls/'.$entry['id'][0].'/'.$entry['id'])."\n"; // append content to output for all aggregated resources
			file_put_contents($this->ro_path.'/cache/resources/'.$filename[0].'/'.$filename,$output,LOCK_EX); // save resource
		};
		$first_entry=key($entries); // get key of first entry
		if ($type=='js') { // if aggregating JS
			$new=$this->xml->createElement('script'); // create script element
			foreach(array('type'=>'text/javascript','src'=>$this->url.'/'.$filename) as $key=>$val) $new->setAttribute($key,$val); // set attributes
		}
		else { // else if aggregating CSS
			$new=$this->xml->createElement('link'); // create link element
			foreach(array('type'=>'text/css','rel'=>'stylesheet','href'=>$this->url.'/'.$filename) as $key=>$val) $new->setAttribute($key,$val); // set attributes
			if (!empty($entries[$first_entry]['media'])) $new->setAttribute('media',$entries[$first_entry]['media']); // set media attribute if available
		};
		if ($type=='js') $entries[$first_entry]['node']->appendChild($this->xml->createTextNode('')); // if this is a script, append text node
		$this->head_cache[$this->xml->saveXML($entries[$first_entry]['node'])]=$this->xml->saveXML($new); // record finds/replacement
		$this->xml->documentElement->replaceChild($new,$entries[$first_entry]['node']); // replace first resource in the DOM with the aggregated resources
		unset($entries[$first_entry]); // remove first resource from array
		if (!empty($entries)) foreach($entries as $entry) { // if any remaining resources, loop through them
			if ($type=='js') $entry['node']->appendChild($this->xml->createTextNode('')); // if this is a script, append text node
			$this->head_cache[$this->xml->saveXML($entry['node'])]=''; // record find/replacement
			$this->xml->documentElement->removeChild($entry['node']); // remove resource from the DOM
		};
	};
};
if (!is_dir($this->ro_path.'/cache/heads/'.$this->head_hash[0])) @mkdir($this->ro_path.'/cache/heads/'.$this->head_hash[0]); // ensure cache dir exists
$contents=serialize($this->head_cache); // serialize head cache
if (@file_get_contents($this->ro_path.'/cache/heads/'.$this->head_hash[0].'/'.$this->head_hash)!=$contents) {@file_put_contents($this->ro_path.'/cache/heads/'.$this->head_hash[0].'/'.$this->head_hash,$contents,LOCK_EX);} // save the cache if changed
else touch($this->ro_path.'/cache/heads/'.$this->head_hash[0].'/'.$this->head_hash); // else reset the cache
if ($this->has_apc) apc_add('ro_head_'.$this->head_hash,$this->head_cache,$this->head_cache_ttl); // if APC available, cache the head
if (!empty($this->expired_urls)) shell_exec('php -f '.$this->ro_path.'/includes/refresh_resources.php '.escapeshellarg($this->document_root).' '.escapeshellarg($this->url).' '.escapeshellarg(serialize($this->expired_urls)).' > /dev/null &'); // spawn async url refresh
}

protected function getContentForResource($resource,$type) { // obtains content for a resource
static $has_java;
$url=$resource['node']->getAttribute($type=='css' ? 'href' : 'src'); // get url for resource
$info=@parse_url($url); // parse url
if (!$info) return false; // if url could not be parsed, return false
$is_external=false; // default to non-external url
if (!empty($info['query']) || !empty($info['host']) || !in_array(pathinfo($info['path'],PATHINFO_EXTENSION),array('js','css'))) { // if url must be loaded externally
	$is_external=true; // set external flag
	if (empty($info['host'])) $url='http'.(isset($_SERVER['HTTPS']) ? 's' : '').'://'.$_SERVER['HTTP_HOST'].($url[0]=='/' ? '' : pathinfo($_SERVER['PHP_SELF'],PATHINFO_DIRNAME).'/').$url; // ensure url has a host
};
$url_cache_id=!empty($info['query']) ? hash('md5',substr($url,0,-1*(strlen($info['query'])+1))).'_'.hash('md5',$info['query']) : hash('md5',$url); // set url cache id
if ($ts=@filemtime($this->ro_path.'/cache/urls/'.$url_cache_id[0].'/'.$url_cache_id.'.md5')) { // if url cache entry is available
	if (!$is_external) if (@filemtime($this->document_root.($url[0]=='/' ? '' : pathinfo($_SERVER['PHP_SELF'],PATHINFO_DIRNAME).'/').$url)>$ts) if ($refresh=$this->refreshResources($url)) foreach($refresh as $key=>$val) $resource[$key]=$val; // force refresh of resource if it is not external and has changed
	$resource['id']=$url_cache_id; // set id and hash for resource
	$resource['md5']=file_get_contents($this->ro_path.'/cache/urls/'.$url_cache_id[0].'/'.$url_cache_id.'.md5');
	if ($is_external && $ts<$_SERVER['REQUEST_TIME']-$this->url_cache_ttl) $this->expired_urls[]=$url; // if external url cache entry has expired, mark it for refresh
	return $resource; // return resource
};
if ($refresh=$this->refreshResources($url)) foreach($refresh as $key=>$val) $resource[$key]=$val; // force refresh of resource if caching for the first time
return $resource; // return content for url if obtained
}

public function refreshResources($urls) { // refreshes a set of resources
static $has_java;
if (!is_array($urls)) $urls=array($urls); // enforce array
if (!empty($urls)) { // if there are urls
	$caches=array(); // init array of cache entries
	foreach($urls as $url) { // for each resource
		if ($info=@parse_url($url)) { // if url parses
			$info['dir']=pathinfo($info['path'],PATHINFO_DIRNAME); // get dirname
			$type=pathinfo($info['path'],PATHINFO_EXTENSION); // get type
			$is_external=(!empty($info['query']) || !empty($info['host']) || !in_array($type,array('js','css'))); // set flag for external urls
			$url_cache_id=!empty($info['query']) ? hash('md5',substr($url,0,-1*(strlen($info['query'])+1))).'_'.hash('md5',$info['query']) : hash('md5',$url); // set url cache id
			$content=$this->doResourceRequest($url,$is_external); // fetch resource
			if ($is_external) if ($content2=$this->doResourceRequest($url,$is_external)) if ($content!=$content2) $content=''; // if resource is external and repeat requests returns different content, don't return content
			if (!empty($content)) { // if there was valid content
				if ($type=='css') { // if CSS, rewrite urls in content
					$matches=array(); // init array of url matches
					preg_match_all('/url\(([^\)]+)\)/',$content,$matches); // match urls
					if (!empty($matches[1])) { // if there were matches
						$find=array(); // init find/replace pairs
						$replace=array();
						foreach($matches[1] as $key=>$url) { // loop through matches
							$find[]=$matches[0][$key]; // configure find/replace pairs							
							$replace[]='url('.($is_external ? $info['scheme'].'://'.$info['host'].(!empty($info['port']) ? ':'.$info['port'] : '') : '').($url[0]!='/' ? $this->getRealPath($info['dir'].(substr($info['dir'],-1,1)!='/' ? '/' : '').$url) : $url).')';
						};
						$content=str_replace($find,$replace,$content); // swap in new urls
					};
				};
				if (strpos($url_cache_id,'_')!==false) { // if this is a query string request
					$tmp=explode('_',$url_cache_id); // split page and query string ids
					$count=0; // init count of query strings for this page
					foreach(glob($this->ro_path.'/cache/urls/'.$tmp[0][0].'/'.$tmp[0].'_*') as $entry) if ($entry=basename($entry)) if (filemtime($this->ro_path.'/cache/urls/'.$entry[0].'/'.$entry)<=$_SERVER['REQUEST_TIME']-$this->url_cache_ttl) {@unlink($this->ro_path.'/cache/urls/'.$entry[0].'/'.$entry);} // count the number of query strings for this page, minus any that are expired
					else if (strpos($entry,'.md5')===false) $count++;
					if ($count>=30) $no_cache=true; // if there are 30 queries to this url already, don't cache another
				};
				if (!isset($no_cache)) $caches[$url_cache_id]=$content; // if allowed to cache, mark it for caching
			};
		};
	};
	if (!empty($caches)) { // if there was content to cache
		if (!isset($has_java)) $has_java=strpos(shell_exec('java -h 2>&1'),'Usage: java')!==false; // set Java flag
		foreach($caches as $url_cache_id=>$content) { // loop through content
			if (!is_dir($this->ro_path.'/cache/urls/'.$url_cache_id[0])) @mkdir($this->ro_path.'/cache/urls/'.$url_cache_id[0]); // make sub dir if it doesn't exist
			$uniqid=uniqid(); // get unique id
			if ($has_java) { // if java available
				@file_put_contents($this->ro_path.'/cache/urls/'.$url_cache_id[0].'/'.$url_cache_id.$uniqid,$content,LOCK_EX); // save the cache
				shell_exec('java -jar '.escapeshellarg($this->ro_path.'/includes/yuicompressor.jar').' --type '.escapeshellarg($type).' '.escapeshellarg($this->ro_path.'/cache/urls/'.$url_cache_id[0].'/'.$url_cache_id.$uniqid).' -o '.escapeshellarg($this->ro_path.'/cache/urls/'.$url_cache_id[0].'/'.$url_cache_id.$uniqid)); // minimize output				
				$content=@file_get_contents($this->ro_path.'/cache/urls/'.$url_cache_id[0].'/'.$url_cache_id.$uniqid); // get minimized content
				unlink($this->ro_path.'/cache/urls/'.$url_cache_id[0].'/'.$url_cache_id.$uniqid); // delete minimized copy
			};
			$hash=hash('md5',$content); // get hash of content
			if ($hash!=@file_get_contents($this->ro_path.'/cache/urls/'.$url_cache_id[0].'/'.$url_cache_id.'.md5')) { // if contents have changed
				@file_put_contents($this->ro_path.'/cache/urls/'.$url_cache_id[0].'/'.$url_cache_id,$content,LOCK_EX); // save the cache
				@file_put_contents($this->ro_path.'/cache/urls/'.$url_cache_id[0].'/'.$url_cache_id.'.md5',$hash,LOCK_EX); // save a hash of the entry
			}
			else { // else if contents have not changed
				touch($this->ro_path.'/cache/urls/'.$url_cache_id[0].'/'.$url_cache_id); // reset the cache
				touch($this->ro_path.'/cache/urls/'.$url_cache_id[0].'/'.$url_cache_id.'.md5');
			};
		};
	};
};
if (sizeof($urls)==1 && !empty($url_cache_id) && !empty($hash) && !empty($content)) return array('id'=>$url_cache_id,'md5'=>$hash); // if this was a single request, return the id and hash
}

protected function getRealPath($path) { // realpath replacement for non-existant files
$arr=array_filter(explode('/',$path),'strlen'); // get string components
$output=array(); // init output array
foreach ($arr as $item) if ($item=='..') {array_pop($output);} // remove component for parent redirection
else if ($item!='.') $output[]=$item; // skip current dir components, add rest to output
return ($path[0]=='/' ? '/' : '').implode('/',$output); // implode new path
}

protected function doResourceRequest($url,$is_external) { // performs a resource request
if (!$is_external || !extension_loaded('curl')) { // if url is not external or curl not available
	$content=@file_get_contents((!$is_external ? $this->document_root.($url[0]=='/' ? '' : pathinfo($_SERVER['PHP_SELF'],PATHINFO_DIRNAME).'/') : '').$url); // get content for resource
	if (!$is_external && empty($content)) $content=$this->doResourceRequest('http'.(isset($_SERVER['HTTPS']) ? 's' : '').'://'.$_SERVER['HTTP_HOST'].($url[0]=='/' ? '' : pathinfo($_SERVER['PHP_SELF'],PATHINFO_DIRNAME).'/').$url,true); // if local request failed, try again as external request in case it is handled virtually
}
else { // else if url is external and curl available
	$ch=curl_init(); // init curl
	curl_setopt($ch,CURLOPT_HEADER,0); // set curl ops
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
	curl_setopt($ch,CURLOPT_USERAGENT,'ResourceOptimizer (http://technologies.whitewhale.net/resource_optimizer)');
	curl_setopt($ch,CURLOPT_COOKIEFILE,$this->ro_path.'/cache/curl_cookie_'.md5($url));
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,5);
	curl_setopt($ch,CURLOPT_TIMEOUT,5);
	curl_setopt($ch,CURLOPT_DNS_USE_GLOBAL_CACHE,1);
	curl_setopt($ch,CURLOPT_DNS_CACHE_TIMEOUT,300);
	curl_setopt($ch,CURLOPT_URL,$url);
	$content=curl_exec($ch); // get content for url
	$code=curl_getinfo($ch,CURLINFO_HTTP_CODE); // check status code
	while (in_array($code,array(301,302,303,307)) && $count<3) { // reattempt up to 3 times if redirect discovered
		curl_setopt($ch,CURLOPT_HEADER,!($code==302 && $count==2)); // set opt to request header
		$content=curl_exec($ch); // re-request content for url
		$matches=array(); // init array of location matches
		preg_match('/Location: (.+?)\s+/',$content,$matches); // match location
		if (!empty($matches[1])) { // if there was a match
			curl_setopt($ch,CURLOPT_HEADER,0); // disable header request
			curl_setopt($ch,CURLOPT_URL,$matches[1]); // set opt to request url supplied via location
			$content=curl_exec($ch); // execute new request
			$code=curl_getinfo($ch,CURLINFO_HTTP_CODE); // get status code
		};
		$count++; // increment counter
		if ($count==3 && $code==302 && !empty($content)) $code=200; // force 200 if 302 returned content
	};
	if (substr($code,0,1)!=2) $content=''; // if not successful request, clear response
	curl_close($ch); // close curl
};
return $content;
}

protected function formatDocument($head,$buffer) { // formats the document for output
if ($this->enable_deferred_load || !empty($this->CDNS)) { // if deferring resource loads or applying CDN(s)
	$this->deferred_js=''; // init deferred JS
	$this->moved_resources=array(); // init moved resources
	if ($this->xml->documentElement->hasChildNodes()) foreach($this->xml->documentElement->childNodes as $node) { // loop through child nodes
		switch($node->nodeName) { // branch by node name
			case 'link': // if link element found
				if (!empty($this->CDNS)) $this->applyCDN($node,'css'); // if applying CDN(s), apply one
				break;
			case 'script': // if script element found
				if (!empty($this->CDNS)) $this->applyCDN($node,'js'); // if applying CDN(s), apply one
				if ($this->enable_deferred_load) $this->deferResource($node,empty($this->no_defers) ? false : true); // if deferring resource loads, defer or move resource
				break;
			case '#comment':
				if ($node->hasChildNodes()) foreach($node->childNodes as $node2) switch($node2->nodeName) { // branch by node name
					case 'link': // if link element found
						if (!empty($this->CDNS)) $this->applyCDN($node2,'css'); // if applying CDN(s), apply one
						break;
					case 'script': // if script element found
						if (!empty($this->CDNS)) $this->applyCDN($node2,'js'); // if applying CDN(s), apply one
						if ($this->enable_deferred_load) $this->deferResource($node2,empty($this->no_defers) ? false : true,true); // if deferring resource loads, defer or move resource
						break;
				};
				break;
		};
	};
	if ($this->enable_deferred_load) if (!empty($this->moved_resources)) foreach($this->moved_resources as $resource) $resource->parentNode->removeChild($resource); // if deferring resource loads, remove moved resources from the DOM
	if (!empty($this->CDNS)) { // if there are CDNs
		$matches=array(); // init array of a tag matches
		preg_match_all('/src="((?!http[s]*:\/\/).+?\.(?:jpg|gif|png))"/',$buffer,$matches); // match locally loaded images
		if (!empty($matches[1])) { // if images were found
			foreach($matches[1] as $val) { // for each match
				$host=$this->CDNS[(strlen($val)+ord((int)substr($val,-5,1)))%sizeof($this->CDNS)]; // choose CDN for this item
				if ($host!=$_SERVER['HTTP_HOST']) $buffer=preg_replace('/src="'.preg_quote($val,'/').'"/','src="http'.(!empty($_SERVER['HTTPS']) ? 's' : '').'://'.$host.($val[0]!='/' ? dirname($_SERVER['PHP_SELF']).'/' : '').$val.'"',$buffer); // replace image src with updated one
			};
		};
	};
};
$new_head=str_replace('<head/>','<head></head>',$this->xml->saveXML()); // get new head
if (strpos($new_head,'<?xml version="1.0"?>')===0) $new_head=substr($new_head,21); // strip xml declaration
$buffer=preg_replace(array('/<head>.*?<\/head>/s','/(?:\s*\n){2,}/s'),array($new_head,"\n"),$buffer); // swap in new head element
if ($this->enable_deferred_load && !empty($this->deferred_js)) $buffer=str_replace('</body>',$this->deferred_js.'</body>',$buffer); // if deferring resources and there is JS being moved, reinsert it
$buffer=trim(preg_replace(array('/\s*data-(no-aggregation|aggregate-break)="[^"]*"/','/<script(.*?)\/\s*>/'),array('','<script\\1></script>'),$buffer)); // clear data-no-aggregation flags, fix any self-closing scripts
return $buffer;
}

protected function deferResource($resource,$is_move=false,$within_comment=false) { // defers a resource via the defer attribute or by moving it to the end of the document
if (!$is_move) { // if not moving the resource
	$resource->setAttribute('defer','defer'); // set attribute
}
else if (!$this->no_moves) { // else if moving resource
	if ($within_comment) $resource=$resource->parentNode; // get parent node instead if resource is within comment
	$this->deferred_js.=$this->xml->saveXML($resource)."\n"; // add deferred JS to defer buffer
	$this->moved_resources[]=$resource; // add moved resource to array
};
}

protected function applyCDN($resource,$type) { // applies a CDN to the resource
$attr_name=$type=='js' ? 'src' : 'href'; // get attr of this type
if ($original=@$resource->getAttribute($attr_name)) { // get original attr value
	if (@parse_url($original,PHP_URL_HOST)=='') { // if attr value parses as url
		$host=$this->CDNS[ord(substr(crc32($original),0,1))%sizeof($this->CDNS)]; // choose CDN for this item
		if ($host!=$_SERVER['HTTP_HOST']) $resource->setAttribute($attr_name,'http'.(!empty($_SERVER['HTTPS']) ? 's' : '').'://'.$host.($original[0]!='/' ? dirname($_SERVER['PHP_SELF']).'/' : '').$original);  // if CDN host is not the current host, set new url on CDN host
	};
};
}

public function cleanCache() { // removes expired entries from the caches
foreach(array('resource'=>$this->ro_path.'/cache/resources','url'=>$this->ro_path.'/cache/urls','head'=>$this->ro_path.'/cache/heads') as $type=>$dir) if (is_dir($dir)) foreach(scandir($dir) as $subdir) if ($subdir[0]!='.') foreach(scandir($dir.'/'.$subdir) as $file) if ($file[0]!='.') if (filemtime($dir.'/'.$subdir.'/'.$file)<=$_SERVER['REQUEST_TIME']-($type=='resource' ? $this->resource_cache_ttl : ($type=='url' ? $this->url_cache_ttl : $this->head_cache_ttl))) unlink($dir.'/'.$subdir.'/'.$file); // loop through cache entries, if not modified since TTL, clear them
}

public function purgeCache() { // purge cache of urls, and then of heads, to prompt end users to get all fresh content (we don't purge resource caches, because they may still be referenced by an external page cache or by static pushed files)
$this->has_apc=extension_loaded('apc'); // if APC available
foreach(array('url'=>$this->ro_path.'/cache/urls','head'=>$this->ro_path.'/cache/heads') as $type=>$dir) if (is_dir($dir)) foreach(scandir($dir) as $subdir) if ($subdir[0]!='.') foreach(scandir($dir.'/'.$subdir) as $file) if ($file[0]!='.') { // loop through cache entries and clear them
	unlink($dir.'/'.$subdir.'/'.$file);
	if ($type=='head' && $this->has_apc) apc_delete('ro_head_'.$file);
};
}

}

?>