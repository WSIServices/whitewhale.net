<?php

$filename=basename($_SERVER['REQUEST_URI']); // get requested file name
$filename=$filename[0].'/'.$filename; // prepend subdir to file name
if (file_exists('./cache/resources/'.$filename)) { // if a cache is available
	$mtime=filemtime('./cache/resources/'.$filename); // get modification time of current cache
	$etag='"'.pathinfo($filename,PATHINFO_FILENAME).'"'; // create etag
	$extension=pathinfo($filename,PATHINFO_EXTENSION); // get extension
	if ((!empty($_SERVER['HTTP_IF_NONE_MATCH']) && str_replace('-gzip','',$_SERVER['HTTP_IF_NONE_MATCH'])==$etag)) $is_etag_response=true; // flag if etag response
	header(!isset($is_etag_response) ? 'HTTP/1.1 200 OK' : 'HTTP/1.1 304 Not Modified'); // send response code
	header('Cache-Control: public, max-age=15552000'); // send cache-control header for +6 months
	header('Last-Modified: '.gmdate('D, d M Y H:i:s',$mtime).' GMT'); // send last-modified header
	header('Expires: '.gmdate('D, d M Y H:i:s',$_SERVER['REQUEST_TIME']+15552000).' GMT'); // send expires header for +6 months
	header('ETag: '.$etag); // send etag header
	header('Content-Type: '.($extension=='js' ? 'text/javascript' : 'text/css')); // send content type
	header('Content-Disposition: inline; filename='.$filename); // send filename
	if (isset($is_etag_response)) { // if handle etag request
		header('Content-Length: 0'); // send zero length header
	    exit; // terminate script
	};
	if (ini_get('zlib.output_compression')!=1) ob_start('ob_gzhandler'); // ensure content is gzipped
	readfile('./cache/resources/'.$filename); // send resource content
}
else header('HTTP/1.1 404 Not Found'); // else return a 404

?>