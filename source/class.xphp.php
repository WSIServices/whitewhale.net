<?php

/*

-------------------------------
XPHP
2004-2010 Alexander Romanovich (alex@whitewhale.net)
v20100726
-------------------------------

For documentation, please see the manual at: http://technologies.whitewhale.net/xphp/

*/

class XPHP {

############## CORE FUNCTIONS ##############

public function __construct($parse=true) { // by default, XPHP will parse the currently executing PHP file, pass in false to avoid doing so
if ($parse) { // if parsing the currently executing file
	if (!ini_get('output_buffering')) ob_start(array($this,'outerBuffer')); // initialize outer output buffer if output buffering is off
	$this->level=ob_get_level(); // get starting level
	ob_start(array($this,'xphp_ob')); // initialize output buffer
	register_shutdown_function(array($this,'execute')); // register the termination and processing of the output buffer as a shutdown function
};
}

public function outerBuffer($buffer) { // close output buffer
return $buffer;
}

public function xphp_ob($buffer) { // copies buffer to class variable for access by shutdown function
$this->buffer=$buffer; // store buffer
return ''; // return empty content for this buffer
}

public function execute() { // close output buffer and process it
while (ob_get_level()!=$this->level) ob_end_flush(); // close out pending buffers before parsing, so we can see errors
echo $this->parseString($this->buffer); // echo the parsed buffer
}

protected function parse($xml) { // transforms an individual xphp object
$this->error='';
$this->xml=$xml[0];
$output='';
$matches=array();
if (preg_match('/<xphp\s+var="([^"]+?)"(?:\s+(?:inner="true"|priority="high""))*\s*\/>/',$xml[0],$matches)) {$output=$GLOBALS[$matches[1]];} // optimization for global vars
else if ($xphp=@simplexml_load_string($xml[0])) { // convert the xphp object to a SimpleXML object for processing
	if (isset($xphp['var'])) $xphp['variable']=$xphp['var']; // alias variable as var
	if (isset($xphp['func'])) $xphp['function']=$xphp['func']; // alias function as func
	if (isset($xphp['disabled']) || (isset($xphp->if) && !$this->ifCond($xphp->if))) { // if xphp object is disabled, or conditions of if statement(s) not satisfied
		$output=isset($xphp->else) ? $this->parseString(str_replace(array('<else ','</else>'),array('<xphp ','</xphp>'),$xphp->else->asXML())) : ''; // set empty content if no else case, otherwise substitute in else object
	}
	else if (isset($xphp['cache']) && ($cache=$this->getCache((string)$xphp['cache'],isset($xphp['apc'])))!==false) {$output=$cache;} // if xphp object has a cache id, and it has been previously cached, set content to cached content
	else { // otherwise process xphp syntax:
		if (isset($xphp['function'])) { // if xphp object requests content from a function
			$object=isset($xphp['object']) ? (string)$xphp['object'] : false; // check if function is an object method
			$output=$this->getOutputFromFunc((string)$xphp['function'],$xphp,$this->getArgs($xphp),$object); // set content to content returned by function
		}
		else if (isset($xphp['variable'])) { // if xphp object requests content from a variable
			if (!isset($xphp['type'])) $xphp['type']=''; // set default type to none (global)
			if (!isset($xphp['cast'])) $xphp['cast']=false; // set default cast to none
			if (!isset($xphp['object'])) $xphp['object']=false; // set default object to none
			$output=$this->getVar((string)$xphp['variable'],(string)$xphp['type'],(string)$xphp['object'],(string)$xphp['cast']); // set content to contents of variable
		}
		else if (isset($xphp['file'])) { // if xphp object should output contents of specified file
			$file=(string)$xphp['file'];
			if ($file[0]!='/') $file=dirname($_SERVER['SCRIPT_FILENAME']).'/'.$file; // prepend full path to file name if relative was given
			if (!file_exists($file)) {$this->error='<p><strong>XPHP: File '.$file.' does not exist.</strong></p>';} // give error if file doesn't exist
			else if (!$output=file_get_contents($file)) $this->error='<p><strong>XPHP: File '.$file.' could not be read.</strong></p>'; // attempt to read file, give error if it can't be read
		}
		else if (isset($xphp['content'])) {$output=$this->getInnerXML($xphp->content);} // if xphp object is xhtml content, set content to that
		else if (isset($xphp['value'])) {$output=(string)$xphp['value'];} // if xphp object is text string, set content to that
		else if (isset($xphp['xslt'])) {$output=$this->XSLTTransform($xphp);} // if xphp object requires XSLT transformation, set content to result of XSLT transformation
		else if (isset($this->registeredTypes)) {$output=$this->parseRegisteredTypes($xphp);} // if this is a custom xphp object, get content for it
		else $output='';
		if (isset($xphp['eval']) && empty($this->error)) $output=$this->parseString($output); // if xphp content must be re-parsed, do it
		if (isset($xphp['cache']) && empty($this->error)) { // if cache id was specified, and it is being requested for the first time, cache the content
			$apc=isset($xphp['apc']); // check if APC should be used
			$ttl=isset($xphp['ttl']) ? (int)$xphp['ttl'] : false; // check if there is a TTL (for APC)
			$this->setCache((string)$xphp['cache'],$output,$apc,$ttl); // cache the content
		};
	};
}
else $this->error='<p><strong>XPHP: Invalid XML</strong><br/>'.htmlentities($xml[0]).'</p>'; // if xml cannot be parsed, give error
return empty($this->error) ? $output : $this->error; // return output or otherwise the error
}

public function getArgs($xphp) { // returns array of args for an xphp object
$args=array();
if (isset($xphp->arg)) foreach($xphp->arg as $arg) { // loop through args if they exist
	$id=(string)$arg['id']; // get arg id
	if (!isset($args[$id])) {$args[$id]=$arg;} // if new arg, add it to arg array
	else if (is_array($args[$id])) {$args[$id][]=$arg;} // if multiple such args exist, add it to array of them
	else $args[$id]=array($args[$id],$arg); // if arg exists, but only one, convert to array and add this one
};
return $args;
}

protected function getOutputFromFunc($function,$xphp,$args,$object) { // fetches content from function output
$output='';
if ($object) { // if function is an object method
	if (method_exists($GLOBALS[$object],$function)) {$output=$GLOBALS[$object]->{$function}($xphp,$args);} // if method exists, set output to content returned by it
	else $this->error='<p><strong>XPHP: Method for '.$function.' does not exist in object '.$object.'.</strong></p>'; // otherwise, give error
}
else if (function_exists($function)) {$output=call_user_func($function,$xphp,$args);} // if function is not an object method, set output to content returned by it
else $this->error='<p><strong>XPHP: Function for '.$function.' does not exist.</strong></p>'; // give error if function does not exist
return $output;
}

protected function ifCond($ifs) { // process if statements for xphp object
$result=true; // default to success
foreach($ifs as $if) if (isset($if['var']) || isset($if['variable'])) { // loop through if statements with a variable name
	if (isset($if['var'])) $if['variable']=$if['var']; // alias variable as var
	if (!isset($if['type'])) {$cmp=isset($GLOBALS[(string)$if['variable']]) ? $GLOBALS[(string)$if['variable']] : '';} // set comparison to global var
	else switch(strtolower((string)$if['type'])) { // if non-global var requested, set comparison to it
		case 'get':$cmp=isset($_GET[(string)$if['variable']]) ? $_GET[(string)$if['variable']] : '';break;
		case 'post':$cmp=isset($_POST[(string)$if['variable']]) ? $_POST[(string)$if['variable']] : '';break;
		case 'server':$cmp=isset($_SERVER[(string)$if['variable']]) ? $_SERVER[(string)$if['variable']] : '';break;
		case 'cookie':$cmp=isset($_COOKIE[(string)$if['variable']]) ? $_COOKIE[(string)$if['variable']] : '';break;
		case 'session':$cmp=isset($_SESSION[(string)$if['variable']]) ? $_SESSION[(string)$if['variable']] : '';break;
		case 'apc':$cmp=apc_fetch($if['variable']);break;
	};
	if ((isset($if['value']) && $cmp!=$if['value']) || (!isset($if['value']) && empty($cmp))) { // fail if a value was specified and value of specified var does not match, or if value was not specified and specified var is null/empty
		$result=false;
		break;
	};
}
else $this->error='<p><strong>XPHP: If statement found with no variable specified.</strong></p>'; // if no variable was specified, give error
return empty($this->error) ? $result : false; // always return false if error, otherwise return result of conditions
}

protected function getVar($var,$type,$object=false,$cast=false) { // fetches content from a variable
switch(strtolower($type)) { // set output to content of requested variable
	case '':$output=isset($GLOBALS[$var]) ? $GLOBALS[$var] : '';break;
	case 'get':$output=isset($_GET[$var]) ? $_GET[$var] : '';break;
	case 'post':$output=isset($_POST[$var]) ? $_POST[$var] : '';break;
	case 'server':$output=isset($_SERVER[$var]) ? $_SERVER[$var] : '';break;
	case 'cookie':$output=isset($_COOKIE[$var]) ? $_COOKIE[$var] : '';break;
	case 'session':$output=isset($_SESSION[$var]) ? $_SESSION[$var] : '';break;
	case 'apc':$output=apc_fetch($var);break;
	case 'object':
		if (empty($object)) {$this->error='<p><strong>XPHP: You must specify an object containing variable '.$var.'.</strong></p>';}
		else if (!isset($GLOBALS[$object])) $this->error='<p><strong>XPHP: Object '.$object.' containing variable '.$var.' does not exist.</strong></p>';
		if (empty($this->error)) $output=isset($GLOBALS[$object]->$var) ? $GLOBALS[$object]->$var : '';
		break;
	default:$output='';break;
};
if ($cast && empty($this->error)) eval('$output=('.$cast.')$output;'); // if cast requested, cast the variable
return $output;
}

public function parseString($str) { // parses xphp objects in a string
$str=preg_replace_callback('/(<arg\s+id=".+?"\s*>)(.*?)(<\/arg>)/s', array($this,'repairAmps'), $str); // repair invalid ampersands
$tag=isset($this->tags) ? '('.implode('|',$this->tags).')' : 'xphp'; // get tags to replace
if (strpos($str,'inner=')!==false || strpos($str,'priority=')) $str=preg_replace('/(<arg\s+id=".+?"\s*>)(.*?)(<\/arg>)/se','"\\1".str_replace("& ","&amp; ","\\2")."\\3"',@preg_replace_callback('/<'.$tag.'\s+[^>]*(?:inner="true"|priority="high").*?(?:\/>|\/*?>.*?<\/'.$tag.'>)/s', array($this,'parse'), $str)); // parse nested template tags in buffer
$str=@preg_replace_callback('/<'.$tag.'(?:\s\w*=".*?"?)*(?:\/>|\/*?>.*?<\/'.$tag.'>)/s', array($this,'parse'), $str); // parse template tags in buffer
return $str;
}

public function parseFile($path,$push_file=false) { // parses xphp objects in a file, optionally pushes resulting content to file
$output='';
if (!file_exists($path)) {$this->error='<strong>XPHP: Supplied path for file parsing is not valid.</strong>';} // if specified file does not exist, give error
else {
	$output=$this->parseString(file_get_contents($path)); // parse the contents of the file
	if ($push_file) { // if content should be pushed to a file
		if (file_exists($push_file)) if (!@unlink($push_file)) $this->error='<strong>XPHP: Could not overwrite existing file '.$push_file.'.</strong>'; // if push file already exists and could not be removed, give error
		if (!@file_put_contents($push_file,$output)) $this->error='<strong>XPHP: Could not push file to '.$push_file.'.</strong>'; // attempt to push file, give error if failed
	};
};
return (!$push_file) ? $output : false; // return parsed content only if not pushing content to file
}

public function registerType($type,$function,$object=false) { // registers a custom, user-defined xphp handler
if (!in_array($type,array('eval','cache','apc','ttl'))) { // if not a reserved type
	if (!isset($this->registeredTypes)) {$this->registeredTypes=array();} // init xphp object type array
	else if (isset($this->registeredTypes[$type])) die('<p><strong>XPHP: Type '.$type.' is already registered.</strong></p>'); // error if type already registered
	$this->registeredTypes[$type]=array($function,$object); // register the type=>function/method pair
};
}

public function registerTag($tag) { // registeres a custom, user-defined xphp tag
if (preg_match('/[a-zA-Z0-9]+/',$tag)) {
	if (!isset($this->tags)) {$this->tags=array('xphp');} // init tags array
	else if (in_array($tag,$this->tags)) die('<p><strong>XPHP: Tag '.$tag.' is already registered.</strong></p>'); // error if tag already registered
	$this->tags[]=$tag;
}
else die('<p><strong>XPHP: Registered tags must only be alphanumeric strings.</strong></p>');
}

protected function parseRegisteredTypes($xphp) { // run through registered xphp object types and handle request if a matching one is found
$output='';
foreach($this->registeredTypes as $type=>$info) if (isset($xphp[$type])) { // scan for matching object type
	if ($info[1] && method_exists($info[1],$info[0])) {$output=$info[1]->{$info[0]}($xphp,$this->getArgs($xphp));} // if method exists, set output to content returned by it
	else if (function_exists($info[0])) {$output=call_user_func($info[0],$xphp,$this->getArgs($xphp));} // if method exists, set output to content returned by it
	else $this->error='<p><strong>XPHP: Function or method for '.$info[0].' does not exist.</strong></p>'; // otherwise, give error
	break;
};
return $output;
}

protected function getInnerXML($sxe) { // extracts inner xml from SimpleXML node
return preg_replace('/>\s*</',">\n<",trim(str_replace(array('<content>','</content>'),'',$sxe->asXML()))); // extract xml for child nodes
}

public function repairAmps($arr) { // repairs invalid ampersands
return $arr[1].str_replace('& ','&amp; ',$arr[2]).$arr[3];
}

############## EXTENSION FUNCTIONS ##############

protected function getCache($cache,$apc=false) { // fetches content for xphp object from cache
return ($apc) ? apc_fetch($cache) : (isset($this->cache[$cache]) ? $this->cache[$cache] : false); // get content from APC if requested, otherwise get content from local cache if exists
}

protected function setCache($cache,$output,$apc,$ttl) { // caches content for xphp object
if ($apc) {apc_store($cache,$output,(int)$ttl);} // cache content with APC if specified
else { // otherwise, cache locally
	if (!isset($this->cache)) $this->cache=array(); // init cache array if not set
	$this->cache[$cache]=$output; // cache content
};
}

protected function XSLTTransform($xphp) { // fetches content from XSLT transformation
$output='';
if (isset($xphp['stylesheet'])) {
	$stylesheet_url=(string)$xphp['stylesheet'];
	if ($stylesheet_url[0]!='/') $stylesheet_url=dirname($_SERVER['SCRIPT_FILENAME']).'/'.$stylesheet_url; // prepend full path to file name if relative was given
	if (!file_exists($stylesheet_url)) {$this->error='<p><strong>XPHP: Supplied stylesheet path is not valid.</strong></p>';} // give error if specified stylesheet does not exist
	else $stylesheet=file_get_contents($stylesheet_url);
}
else if (isset($xphp->stylesheet)) $stylesheet=$this->getInnerXML($xphp->stylesheet); // otherwise get inline stylesheet xml
if (isset($xphp['xml'])) {
	$xml_url=(string)$xphp['xml'];
	if ($xml_url[0]!='/') $xml_url=dirname($_SERVER['SCRIPT_FILENAME']).'/'.$xml_url; // prepend full path to file name if relative was given
	if (!file_exists($xml_url)) {$this->error='<p><strong>XPHP: Supplied xml path is not valid.</strong></p>';} // give error if specified xml does not exist
	else $xml=file_get_contents($xml_url);
}
else if (isset($xphp->xml)) $xml=$this->getInnerXML($xphp->xml); // else get inline xml if supplied
if (empty($this->error)) if (!isset($stylesheet) || empty($stylesheet)) {$this->error='<p><strong>XPHP: You must supply a stylesheet for the XSLT transformation.</strong></p>';}
else if (!isset($xml) || empty($xml)) {$this->error='<p><strong>XPHP: You must provide the xml for XSLT to transform.</strong></p>';}
else {
	$output='';
	$proc=new XSLTProcessor(); // create new xslt processor
	$proc->registerPHPFunctions(); // register php functions for processor
	$source=new DOMDocument(); // create new dom document
	$source->loadXML($xml); // load xml into dom document
	$xsl=new DOMDocument(); // create dom document for xsl transformation
	if ($xsl->loadXML($stylesheet)) { // load the stylesheet
		if (!empty($_GET)) foreach($_GET as $key=>$val) $proc->setParameter('','get_'.$key,$val); // import get vars
		$proc->importStylesheet($xsl);
		$doc=$proc->transformToDoc($source); // import the stylesheet and transform
		$doc->formatOutput=true;
		$output=$doc->saveXML(); // convert to xml output
	}
	else $this->error='<p><strong>XPHP: Could not load stylesheet '.$stylesheet.'.</strong></p>';
};
return $output;
}

};

?>